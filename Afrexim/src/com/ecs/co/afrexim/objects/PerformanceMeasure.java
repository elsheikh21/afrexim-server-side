package com.ecs.co.afrexim.objects;

public class PerformanceMeasure {

	private String measureGuid;
	private String measureName;
	private String measureDescription;
	private String measureWeight;
	private String importMappingkey;
	private String thresholdWorst;
	private String thresholdRedFlag;
	private String thresholdGoal;
	private String thresholdBest;

	public String getMeasureGuid() {
		return measureGuid;
	}

	public void setMeasureGuid(String measureGuid) {
		this.measureGuid = measureGuid;
	}

	public String getMeasureName() {
		return measureName;
	}

	public void setMeasureName(String measureName) {
		this.measureName = measureName;
	}

	public String getMeasureDescription() {
		return measureDescription;
	}

	public void setMeasureDescription(String measureDescription) {
		this.measureDescription = measureDescription;
	}

	public String getMeasureWeight() {
		return measureWeight;
	}

	public void setMeasureWeight(String measureWeight) {
		this.measureWeight = measureWeight;
	}

	public String getImportMappingkey() {
		return importMappingkey;
	}

	public void setImportMappingkey(String importMappingkey) {
		this.importMappingkey = importMappingkey;
	}

	public String getThresholdWorst() {
		return thresholdWorst;
	}

	public void setThresholdWorst(String thresholdWorst) {
		this.thresholdWorst = thresholdWorst;
	}

	public String getThresholdRedFlag() {
		return thresholdRedFlag;
	}

	public void setThresholdRedFlag(String thresholdRedFlag) {
		this.thresholdRedFlag = thresholdRedFlag;
	}

	public String getThresholdGoal() {
		return thresholdGoal;
	}

	public void setThresholdGoal(String thresholdGoal) {
		this.thresholdGoal = thresholdGoal;
	}

	public String getThresholdBest() {
		return thresholdBest;
	}

	public void setThresholdBest(String thresholdBest) {
		this.thresholdBest = thresholdBest;
	}

}