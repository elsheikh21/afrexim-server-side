
package com.ecs.co.afrexim.objects;

import java.util.List;

public class QuickScore {

	private int scorecardId;
	private String departmentName;
	private List<StrategicObjective> strategicObjectives = null;

	public int getScorecardId() {
		return scorecardId;
	}

	public void setScorecardId(int scorecardId) {
		this.scorecardId = scorecardId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public List<StrategicObjective> getStrategicObjectives() {
		return strategicObjectives;
	}

	public void setStrategicObjectives(List<StrategicObjective> strategicObjectives) {
		this.strategicObjectives = strategicObjectives;
	}


}