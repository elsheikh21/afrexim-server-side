
package com.ecs.co.afrexim.objects;

public class StrategicInitiative {

	private String initiativeGuid;
	private String initiativeName;
	private String startDate;
	private String dueDate;

	public String getInitiativeGuid() {
		return initiativeGuid;
	}

	public void setInitiativeGuid(String initiativeGuid) {
		this.initiativeGuid = initiativeGuid;
	}

	public String getInitiativeName() {
		return initiativeName;
	}

	public void setInitiativeName(String initiativeName) {
		this.initiativeName = initiativeName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

}