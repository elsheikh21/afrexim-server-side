
package com.ecs.co.afrexim.objects;

import java.util.List;

public class StrategicObjective {

	private String objectiveGuid;
	private String objectiveCategory;
	private String objectiveName;
	private String objectiveDescription;
	private String objectiveWeight;
	private List<PerformanceMeasure> performanceMeasures = null;
	private List<StrategicInitiative> strategicInitiatives = null;

	public String getObjectiveGuid() {
		return objectiveGuid;
	}

	public void setObjectiveGuid(String objectiveGuid) {
		this.objectiveGuid = objectiveGuid;
	}

	public String getObjectiveCategory() {
		return objectiveCategory;
	}

	public void setObjectiveCategory(String objectiveCategory) {
		this.objectiveCategory = objectiveCategory;
	}

	public String getObjectiveName() {
		return objectiveName;
	}

	public void setObjectiveName(String objectiveName) {
		this.objectiveName = objectiveName;
	}


	public String getObjectiveDescription() {
		return objectiveDescription;
	}

	public void setObjectiveDescription(String objectiveDescription) {
		this.objectiveDescription = objectiveDescription;
	}

	public String getObjectiveWeight() {
		return objectiveWeight;
	}

	public void setObjectiveWeight(String objectiveWeight) {
		this.objectiveWeight = objectiveWeight;
	}


	public List<PerformanceMeasure> getPerformanceMeasures() {
		return performanceMeasures;
	}

	public void setPerformanceMeasures(List<PerformanceMeasure> performanceMeasures) {
		this.performanceMeasures = performanceMeasures;
	}

	public List<StrategicInitiative> getStrategicInitiatives() {
		return strategicInitiatives;
	}

	public void setStrategicInitiatives(List<StrategicInitiative> strategicInitiatives) {
		this.strategicInitiatives = strategicInitiatives;
	}


}