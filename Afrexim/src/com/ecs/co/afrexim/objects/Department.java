package com.ecs.co.afrexim.objects;

public class Department {

	private String scorecardGuid;
	private String departmentName;

	public String getScorecardGuid() {
		return scorecardGuid;
	}

	public void setScorecardGuid(String scorecardGuid) {
		this.scorecardGuid = scorecardGuid;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

}