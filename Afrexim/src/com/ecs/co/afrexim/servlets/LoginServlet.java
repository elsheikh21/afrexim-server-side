package com.ecs.co.afrexim.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ecs.co.afrexim.dao.QuickScoreDAO;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginServlet.class);
	private final List<String> allowedOrigins = Arrays.asList("");
	private static final long serialVersionUID = 1L;
	private QuickScoreDAO quickscoreDAO;

	// public void init(ServletConfig config) throws ServletException {

	// }

	// protected void doGet(HttpServletRequest request, HttpServletResponse response)
	// 		throws ServletException, IOException {

	// }

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Access-Control-Allow-Origin
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
		response.setHeader("Vary", "Origin");
		// Access-Control-Max-Age
		response.setHeader("Access-Control-Max-Age", "3600");
		// Access-Control-Allow-Credentials
		response.setHeader("Access-Control-Allow-Credentials", "true");
		// Access-Control-Allow-Methods
		response.setHeader("Access-Control-Allow-Methods", "POST");
		// Access-Control-Allow-Headers
		response.setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");
		response.setContentType("application/json;charset=UTF-8");
		try {
			JSONObject jo = new JSONObject();
			jo.put("status", doLogin(request));
			response.getWriter().write(jo.toString());
		} catch (ParseException | SQLException | JSONException e) {
			response.getWriter().println("PLogging in failed with reason: " + e.getMessage());
			LOGGER.error("Logging in Operation failed: ", e);
		}
	}

	private String doLogin(HttpServletRequest request)
			throws ServletException, IOException, SQLException, ParseException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String apiKey = request.getParameter("apiKey");
		final Pattern emailAddressREGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
				Pattern.CASE_INSENSITIVE);
		Matcher matcher = emailAddressREGEX.matcher(email);
		if (email != null && password != null && !email.trim().isEmpty() && !password.trim().isEmpty() && apiKey != null
				&& matcher.matches()) {
			String token = quickscoreDAO.startAPISession();
			if (!token.isEmpty()) {
				HttpSession session = request.getSession(true);
				if (!session.isNew()) {
					session.invalidate();
					session = request.getSession(true);
				}
				session.setMaxInactiveInterval(15 * 60);
				session.setAttribute("Token", token);
				session.setAttribute("APIKey", apiKey);
			}
		}
		return "Invalid entries.";
	}
}
