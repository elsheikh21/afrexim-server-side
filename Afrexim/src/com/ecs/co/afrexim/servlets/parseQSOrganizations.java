package com.ecs.co.afrexim.servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

// import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ecs.co.afrexim.dao.QuickScoreDAO;

@WebServlet("/parseorg")
public class ParseQSOrganizations extends HttpServlet {
	private static final Logger LOGGER = LoggerFactory.getLogger(parseQSOrganizations.class);
	private final List<String> allowedOrigins = Arrays.asList("");
	private static final long serialVersionUID = 1L;
	private QuickScoreDAO quickscoreDAO;

	// public void init(ServletConfig config) throws ServletException {
		
	// }

	// protected void doGet(HttpServletRequest request, HttpServletResponse response)
	// 		throws ServletException, IOException {

	// }

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Access-Control-Allow-Origin
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
		response.setHeader("Vary", "Origin");
		// Access-Control-Max-Age
		response.setHeader("Access-Control-Max-Age", "3600");
		// Access-Control-Allow-Credentials
		response.setHeader("Access-Control-Allow-Credentials", "true");
		// Access-Control-Allow-Methods
		response.setHeader("Access-Control-Allow-Methods", "POST");
		// Access-Control-Allow-Headers
		response.setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");
		response.setContentType("application/json;charset=UTF-8");
		try {
			JSONObject jo = new JSONObject();
			jo.put("status", doGetQSOrganizations(request));
			response.getWriter().write(jo.toString());
		} catch (ParseException | JSONException e) {
			response.getWriter().println("Parsing Quickscore Organization failed with reason: " + e.getMessage());
			LOGGER.error("Parsing Quickscore Organization Operation failed: ", e);
		}
	}

	private String doGetQSOrganizations(HttpServletRequest request) throws ParseException, IOException {
		HttpSession session = request.getSession(false);
		if (session != null) {
			String token = (String) session.getServletContext().getAttribute("Token");
			if (token != null) {
				return quickscoreDAO.getQSOrganization(token);
			}
		}
		return "Invalid operation.";
	}
}
