package com.ecs.co.afrexim.servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet("/logout")
public class LogOutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final List<String> allowedOrigins = Arrays.asList("");
	private static final Logger LOGGER = LoggerFactory.getLogger(LogOutServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doPost(request, response);
		} catch (Exception e) {
			response.getWriter().println("Operation failed with reason: " + e);
			LOGGER.error("Operation failed", e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Access-Control-Allow-Origin
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
		response.setHeader("Vary", "Origin");
		// Access-Control-Max-Age
		response.setHeader("Access-Control-Max-Age", "3600");
		// Access-Control-Allow-Credentials
		response.setHeader("Access-Control-Allow-Credentials", "true");
		// Access-Control-Allow-Methods
		response.setHeader("Access-Control-Allow-Methods", "POST, GET");
		// Access-Control-Allow-Headers
		response.setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");
		response.setContentType("application/json;charset=UTF-8");
		try {
			JSONObject jo = new JSONObject();
			HttpSession session = request.getSession();
			if (session != null) {
				String token = (String) session.getServletContext().getAttribute("Token");
				if (token != null) {
					session.invalidate();
				}
			}
			jo.put("status", "-100");
			response.getWriter().write(jo.toString());
		} catch (JSONException e) {
			response.getWriter().println("Operation failed with reason: " + e);
			LOGGER.error("Operation failed", e);
		}
	}

}
