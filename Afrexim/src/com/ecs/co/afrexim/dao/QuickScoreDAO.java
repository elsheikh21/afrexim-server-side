package com.ecs.co.afrexim.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Properties;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.ecs.co.afrexim.objects.Department;
import com.ecs.co.afrexim.objects.PerformanceMeasure;
import com.ecs.co.afrexim.objects.QuickScore;
import com.ecs.co.afrexim.objects.StrategicInitiative;
import com.ecs.co.afrexim.objects.StrategicObjective;

public class QuickScoreDAO {
	private String email;
	private String password;
	private String apiKey;
	
	public QuickScoreDAO() {
		Properties configFile = new Properties();
		InputStream input = null;
		input = getClass().getClassLoader().getResourceAsStream("config.properties");
		try {
			configFile.load(input);
			email = configFile.getProperty("email");
			password = configFile.getProperty("password");
			apiKey = configFile.getProperty("apiKey");
		} catch (IOException exc) {
			System.out.println(exc.getMessage());
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException exc) {
					System.out.println(exc.getMessage());
				}
			}
		}
	}
	
	public String startAPISession() throws ParseException, IOException {
		String uri = "https://api.bsctoolkit.net/core/Public/authenticateAPIUser?apikey="
				+ java.net.URLEncoder.encode(apiKey, "UTF-8");
		URL url = new URL(uri);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setConnectTimeout(2 * 60 * 1000);
		connection.setDoOutput(true);
		byte[] out = "{\"Username\":\"root\",\"Password\":\"password\"}".getBytes(StandardCharsets.UTF_8);

		String requestBody = new String(out).replace("root", email).replace("password", password);
		byte[] reqBody = requestBody.getBytes(StandardCharsets.UTF_8);
		int length = reqBody.length;

		connection.setFixedLengthStreamingMode(length);
		connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		connection.setRequestProperty("Accept", "application/json");
		try (OutputStream os = connection.getOutputStream()) {
			os.write(reqBody);
		}
		connection.connect();
		OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
		osw.close();

		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		String responseResult = response.toString();

		JSONParser parser = new JSONParser();
		JSONObject jsonObj = (JSONObject) parser.parse(responseResult);

		if (!jsonObj.get("success").equals(true))
			return "";

		JSONObject jsonDataObj = (JSONObject) jsonObj.get("data");
		if ((Math.toIntExact((Long) jsonDataObj.get("StatusCode"))) == (1)) {
			String token = (String) jsonDataObj.get("Token");
			if (token != null)
				return token;
		}
		return "";
	}

	public String getQSOrganization(String token) throws ParseException, IOException {
		String uri = "https://api.bsctoolkit.net/qs/Tier2/getTier2Organizations?apikey="
				+ java.net.URLEncoder.encode(apiKey, "UTF-8") + "&token=" + java.net.URLEncoder.encode(token, "UTF-8");
		URL url = new URL(uri);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setConnectTimeout(2 * 60 * 1000);
		connection.setDoOutput(true);

		connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		connection.setRequestProperty("Accept", "application/json");
		connection.connect();

		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		String responseResult = response.toString();

		JSONParser parser = new JSONParser();
		JSONObject jsonObj = (JSONObject) parser.parse(responseResult);

		if (!jsonObj.get("success").equals(true))
			return "Error";
		ArrayList<Department> departments = new ArrayList<Department>();
		JSONArray jsonArr = (JSONArray) jsonObj.get("data");
		for (int i = 0; i < jsonArr.size(); i++) {
			jsonObj = (JSONObject) jsonArr.get(i);
			Department dep = new Department();
			String scoreCardUID = String.valueOf((Long) jsonObj.get("ScorecardGuid"));
			String departmentName = (String) jsonObj.get("DepartmentName");
			dep.setScorecardGuid(scoreCardUID);
			dep.setDepartmentName(departmentName);
			departments.add(dep);
		}
		if (departments.isEmpty())
			return "Error";
		return "Success";
	}

	public String getQSScoreCards(String token) throws IOException, ParseException {
		String uri = "https://api.bsctoolkit.net/qs/Tier2/getTier2Scorecards?apikey="
				+ java.net.URLEncoder.encode(apiKey, "UTF-8") + "&token=" + java.net.URLEncoder.encode(token, "UTF-8");
		URL url = new URL(uri);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setConnectTimeout(2 * 60 * 1000);
		connection.setDoOutput(true);

		connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		connection.setRequestProperty("Accept", "application/json");
		connection.connect();

		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		String responseResult = response.toString();

		JSONParser parser = new JSONParser();
		JSONObject jsonObj = (JSONObject) parser.parse(responseResult);
		if (!jsonObj.get("success").equals(true))
			return "Error.";

		QuickScore qs = new QuickScore();
		JSONArray dataJsonArr = (JSONArray) jsonObj.get("data");
		for (int i = 0; i < dataJsonArr.size(); i++) {
			JSONObject jsonObject = (JSONObject) dataJsonArr.get(i);
			Long scoreCardID = (Long) jsonObject.get("ScorecardGuid");
			String departmentName = (String) jsonObject.get("DepartmentName");

			qs.setScorecardId(Math.toIntExact(scoreCardID));
			qs.setDepartmentName(departmentName);

			ArrayList<StrategicObjective> strategicObjectives = new ArrayList<StrategicObjective>();

			JSONArray jsonArr = (JSONArray) jsonObject.get("StrategicObjectives");
			for (int j = 0; j < jsonArr.size(); j++) {
				JSONObject strategicObj = (JSONObject) jsonArr.get(j);

				StrategicObjective strategicObjective = new StrategicObjective();

				strategicObjective.setObjectiveGuid((String) strategicObj.get("ObjectiveGuid"));
				strategicObjective.setObjectiveCategory((String) strategicObj.get("ObjectiveCategory"));
				strategicObjective.setObjectiveName((String) strategicObj.get("ObjectiveName"));
				strategicObjective.setObjectiveDescription((String) strategicObj.get("ObjectiveDescription"));
				strategicObjective.setObjectiveWeight((String) strategicObj.get("ObjectiveWeight"));

				ArrayList<PerformanceMeasure> performanceMeasureList = new ArrayList<PerformanceMeasure>();
				JSONArray performanceMeasures = (JSONArray) strategicObj.get("PerformanceMeasures");
				for (int k = 0; k < performanceMeasures.size(); k++) {
					JSONObject performanceMeasure = (JSONObject) performanceMeasures.get(k);

					PerformanceMeasure performanceMeasureObj = new PerformanceMeasure();

					performanceMeasureObj.setMeasureGuid((String) performanceMeasure.get("MeasureGuid"));
					performanceMeasureObj.setMeasureName((String) performanceMeasure.get("MeasureName"));
					performanceMeasureObj.setMeasureWeight((String) performanceMeasure.get("MeasureWeight"));
					performanceMeasureObj.setThresholdBest((String) performanceMeasure.get("ThresholdBest"));
					performanceMeasureObj.setThresholdGoal((String) performanceMeasure.get("ThresholdGoal"));
					performanceMeasureObj.setThresholdWorst((String) performanceMeasure.get("ThresholdWorst"));
					performanceMeasureObj.setThresholdRedFlag((String) performanceMeasure.get("ThresholdRedFlag"));
					performanceMeasureObj.setImportMappingkey((String) performanceMeasure.get("ImportMappingkey"));
					performanceMeasureObj.setMeasureDescription((String) performanceMeasure.get("MeasureDescription"));

					performanceMeasureList.add(performanceMeasureObj);
				}

				strategicObjective.setPerformanceMeasures(performanceMeasureList);

				ArrayList<StrategicInitiative> strategicIntiativesList = new ArrayList<StrategicInitiative>();
				JSONArray strategicIntiatives = (JSONArray) strategicObj.get("StrategicInitiatives");
				for (int m = 0; m < strategicIntiatives.size(); m++) {
					JSONObject strategicIntJsonObj = (JSONObject) strategicIntiatives.get(m);

					StrategicInitiative strategicInitiativeObject = new StrategicInitiative();

					strategicInitiativeObject.setDueDate((String) strategicIntJsonObj.get("DueDate"));
					strategicInitiativeObject.setStartDate((String) strategicIntJsonObj.get("StartDate"));
					strategicInitiativeObject.setInitiativeGuid((String) strategicIntJsonObj.get("InitiativeGuid"));
					strategicInitiativeObject.setInitiativeName((String) strategicIntJsonObj.get("InitiativeName"));

					strategicIntiativesList.add(strategicInitiativeObject);
				}
				strategicObjective.setStrategicInitiatives(strategicIntiativesList);
			}
			qs.setStrategicObjectives(strategicObjectives);
		}
		if (qs.getStrategicObjectives().isEmpty())
			return "Error.";
		return "Success";
	}
}