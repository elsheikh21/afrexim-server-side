package com.ecs.co.afrexim.dao;

import java.net.URL;
import java.util.Properties;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.net.ssl.HttpsURLConnection;
import com.sun.xml.internal.messaging.saaj.util.Base64;

import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class SuccessFactorsDAO {

	private String username;
	private String password;
	private String odataUrlSimpleGoal;
	private String odataUrlAll;

	public SuccessFactorsDAO() throws IOException {
		Properties configFile = new Properties();
		InputStream input = null;
		input = getClass().getClassLoader().getResourceAsStream("config.properties");
		try {
			configFile.load(input);
			username = configFile.getProperty("odatausername");
			password = configFile.getProperty("odatapassword");
			odataUrlSimpleGoal = configFile.getProperty("odataUrlSimpleGoal");
			odataUrlAll = configFile.getProperty("odataUrlAll");
		} finally {
			if (input != null) {
				input.close();
			}
		}
	}

	public void consumeODataSimpleGoal() throws ParseException, IOException, JSONException {
		String authString = username + ":" + password;
		byte[] authEncBytes = Base64.encode(authString.getBytes());
		String authStringEnc = new String(authEncBytes);

		String uri = odataUrlSimpleGoal;
		URL url = new URL(uri);

		HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setConnectTimeout(2 * 60 * 1000);
		connection.setDoOutput(true);

		connection.setRequestProperty("Authorization", "Basic " + authStringEnc);
		connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		connection.setRequestProperty("Accept", "application/json");

		connection.connect();

		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		String responseResult = response.toString();
		parseSimpleGoalXML(responseResult);
	}

	public void parseSimpleGoalXML(String xmlObj) throws JSONException, ParseException {
		JSONParser parser = new JSONParser();
		JSONObject jsonObj = (JSONObject) parser.parse(xmlObj);
		jsonObj = (JSONObject) jsonObj.get("d");
		JSONArray jsonArr = (JSONArray) jsonObj.get("results");
		for (int i = 0; i < jsonArr.size(); i++) {
			System.out.println("Object #0" + (i + 1));
			JSONObject jsonObject = (JSONObject) jsonArr.get(i);
			boolean flag = (boolean) ((Long) jsonObject.get("flag") == 1);
			String name = (String) jsonObject.get("name");
			String id = (String) jsonObject.get("id");				
			String type = (String) jsonObject.get("type");
			String userID = (String) jsonObject.get("userId");
			System.out.println("Flag: " + flag + ", \n" + "Name: " + name + ", \n" + "ID: " + id + ", \n" + "Type: "
					+ type + ", \n" + "User ID: " + userID + ". \n");
			System.out.println("--------------------------------------------");
		}
	}

	public void consumeOData() throws ParseException {
		try {
			String authString = username + ":" + password;
			byte[] authEncBytes = Base64.encode(authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			String uri = odataUrlAll;
			URL url = new URL(uri);
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setConnectTimeout(2 * 60 * 1000);
			connection.setDoOutput(true);

			connection.setRequestProperty("Authorization", "Basic " + authStringEnc);
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			connection.setRequestProperty("Accept", "application/json");

			connection.connect();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			String responseResult = response.toString();
			parseXML(responseResult);
		} catch (JSONException | IOException exc) {
			System.out.println(exc);
		}
	}

	public void parseXML(String xmlObj) throws JSONException, ParseException {
		JSONParser parser = new JSONParser();
		JSONObject jsonObj = (JSONObject) parser.parse(xmlObj);
		JSONArray jsonArr = (JSONArray) jsonObj.get("EntitySets");
		for (int i = 0; i < jsonArr.size(); i++) {
			System.out.println("Entity #0" + (i + 1));
			String entity = (String) jsonArr.get(i);
			System.out.println(entity + " \n");
			System.out.println("--------------------------------------------");
		}
	}

}
